<?php

namespace App\Controller;

use App\Consts\General;
use App\Utils\JsonResponseConfig;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class GenericController extends AbstractController implements General
{
    /** @var string|null $entity */
    var $entity = null;

    public function index(): JsonResponse
    {
        return $this->baseIndex(
            $this->getRepo()->findAll()
        );
    }

    public function baseIndex(array $results): JsonResponse
    {
        return $this->returnJson([
            static::DATA => $results,
        ]);
    }

    public function returnJson(array $param, int $code = Response::HTTP_OK, string $statusText = self::OK): JsonResponse
    {
        $response = JsonResponseConfig::getJsonResponse();
        $response->setData($param);
        $response->setStatusCode($code, $statusText);

        return $response;
    }

    protected function getRepo(?string $entityName = null): EntityRepository
    {
        return $this->getDoctrine()->getManager()->getRepository($entityName ?? $this->entity);
    }
}
