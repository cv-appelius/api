<?php

namespace App\Controller\History;

use App\Entity\History\Education;

final class EducationController extends Controller
{
    var $entity = Education::class;
}
