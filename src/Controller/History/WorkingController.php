<?php

namespace App\Controller\History;

use App\Entity\History\Working;
use Symfony\Component\HttpFoundation\JsonResponse;

final class WorkingController extends Controller
{
    var $entity = Working::class;

    public function index(): JsonResponse
    {
        return $this->baseIndex(
            $this->getRepo()->findBy([], [
                static::RANK => static::DESC,
            ])
        );
    }
}
