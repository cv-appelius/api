<?php

namespace App\Controller\History;

use App\Entity\History\Abstraction;
use App\Controller\GenericController;

class Controller extends GenericController
{
    var $entity = Abstraction::class;
}
