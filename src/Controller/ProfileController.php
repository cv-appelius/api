<?php

namespace App\Controller;

use App\Entity\Profile;
use Symfony\Component\HttpFoundation\JsonResponse;

final class ProfileController extends GenericController
{
    var $entity = Profile::class;

    public function index(): JsonResponse
    {
        return $this->baseIndex(
            $this->getRepo()->findBy([], [], 1)
        );
    }
}
