<?php

namespace App\Controller;

use App\Entity\Skills;
use Symfony\Component\HttpFoundation\JsonResponse;

final class SkillsController extends GenericController
{
    var $entity = Skills::class;

    public function index(): JsonResponse
    {
        return $this->baseIndex(
            $this->getRepo()->findBy([], [
                static::PERCENTAGE => static::DESC,
            ])
        );
    }
}
