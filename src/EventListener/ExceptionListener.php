<?php

namespace App\EventListener;

use App\Consts\General;
use App\Utils\JsonResponseConfig;
use App\Utils\Validations;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

final class ExceptionListener implements General
{
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        // You get the exception object from the received event
        $exception = $event->getThrowable();

        $response = JsonResponseConfig::getJsonResponse();

        $statusCode = 0;

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
        if ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
            $response->headers->replace($exception->getHeaders());
        } else {
            $statusCode = $exception->getCode();
        }

        $statusCode = Validations::correctStatusCode(!is_numeric($statusCode) ? 0 : $statusCode);

        $response->setStatusCode($statusCode);
        $response->setData([
            static::MESSAGE => $exception->getMessage(),
        ]);

        $this->logger->critical($exception->getMessage());
        $this->logger->critical($exception->getTraceAsString());

        // sends the modified response object to the event
        $event->setResponse($response);
    }
}
