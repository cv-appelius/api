<?php

namespace App\Consts;

interface General
{
    const DATA       = "data";
    const DESC       = "DESC";
    const MESSAGE    = "message";
    const OK         = "ok";
    const PERCENTAGE = "percentage";
    const RANK       = "rank";
}
