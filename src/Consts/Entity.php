<?php

namespace App\Consts;

interface Entity
{
    const BIRTHDAY    = "birthday";
    const DESCRIPTION = "description";
    const EMAIL       = "email";
    const FROM        = "from";
    const JOB_TITLE   = "jobTitle";
    const NAME        = "name";
    const PERCENT     = "percent";
    const TO          = "to";
}
