<?php

namespace App\Traits\Entity;

trait description
{
    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
}
