<?php

namespace App\Traits\Entity;

trait jobTitle
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $jobTitle;

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }
}
