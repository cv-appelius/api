<?php

namespace App\Repository\History;

use App\Entity\History\Working;

/**
 * @method Working|null find($id, $lockMode = null, $lockVersion = null)
 * @method Working|null findOneBy(array $criteria, array $orderBy = null)
 * @method Working[]    findAll()
 * @method Working[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class WorkingRepository extends Repository
{
    const ENTITY = Working::class;
}
