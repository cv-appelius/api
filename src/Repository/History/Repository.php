<?php

namespace App\Repository\History;

use App\Entity\History\Abstraction;
use App\Repository\GenericRepository;

/**
 * @method Abstraction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Abstraction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Abstraction[]    findAll()
 * @method Abstraction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Repository extends GenericRepository
{
    const ENTITY = Abstraction::class;
}
