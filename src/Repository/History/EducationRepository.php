<?php

namespace App\Repository\History;

use App\Entity\History\Education;

/**
 * @method Education|null find($id, $lockMode = null, $lockVersion = null)
 * @method Education|null findOneBy(array $criteria, array $orderBy = null)
 * @method Education[]    findAll()
 * @method Education[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class EducationRepository extends Repository
{
    const ENTITY = Education::class;
}
