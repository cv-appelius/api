<?php

namespace App\Repository;

use App\Entity\Skills;

/**
 * @method Skills|null find($id, $lockMode = null, $lockVersion = null)
 * @method Skills|null findOneBy(array $criteria, array $orderBy = null)
 * @method Skills[]    findAll()
 * @method Skills[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class SkillsRepository extends GenericRepository
{
    const ENTITY = Skills::class;
}
