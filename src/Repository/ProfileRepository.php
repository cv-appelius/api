<?php

namespace App\Repository;

use App\Entity\Profile;

/**
 * @method Profile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Profile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Profile[]    findAll()
 * @method Profile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class ProfileRepository extends GenericRepository
{
    const ENTITY = Profile::class;
}
