<?php

namespace App\Utils;

use Symfony\Component\HttpFoundation\JsonResponse;

final class JsonResponseConfig
{
    const CONTENT_TYPE              = "Content-Type";
    const CONTENT_TYPE_CHARSET_UTF8 = "; charset=UTF-8";

    static function getJsonResponse(): JsonResponse
    {
        $response = new JsonResponse();
        $response->headers->set(
            static::CONTENT_TYPE, 
            $response->headers->get(static::CONTENT_TYPE).static::CONTENT_TYPE_CHARSET_UTF8
        );
        $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

        return $response;
    }
}
