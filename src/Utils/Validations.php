<?php

namespace App\Utils;

final class Validations
{
    static function correctStatusCode(int $statusCode): string
    {
        return $statusCode <= 0 ? 500 : $statusCode;
    }
}
