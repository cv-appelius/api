<?php

namespace App\Entity;

use App\Traits\Entity\description;
use App\Traits\Entity\jobTitle;
use App\Traits\Entity\name;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfileRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Profile extends GenericEntity
{
    use name, description, jobTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $email;

    /**
     * @ORM\Column(type="date")
     */
    protected $birthdate;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function jsonData(): array
    {
        return [
            static::BIRTHDAY    => $this->getBirthdate(),
            static::DESCRIPTION => $this->getDescription(),
            static::EMAIL       => $this->getEmail(),
            static::JOB_TITLE   => $this->getJobTitle(),
            static::NAME        => $this->getName(),
        ];
    }
}
