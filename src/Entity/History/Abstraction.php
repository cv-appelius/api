<?php

namespace App\Entity\History;

use App\Entity\GenericEntity;
use App\Traits\Entity\description;
use App\Traits\Entity\jobTitle;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\History\Repository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="history")
 *
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(
 *     name="type",
 *     type="string"
 * )
 *
 * @ORM\DiscriminatorMap({
 *     "working"   = "App\Entity\History\Working",
 *     "education" = "App\Entity\History\Education",
 * })
 */
abstract class Abstraction extends GenericEntity
{
    use description, jobTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $isFrom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $untilTo;

    public function getIsFrom(): ?string
    {
        return $this->isFrom;
    }

    public function setIsFrom(string $isFrom): self
    {
        $this->isFrom = $isFrom;

        return $this;
    }

    public function getUntilTo(): ?string
    {
        return $this->untilTo;
    }

    public function setUntilTo(string $untilTo): self
    {
        $this->untilTo = $untilTo;

        return $this;
    }

    public function jsonData(): array
    {
        return [
            static::DESCRIPTION => $this->getDescription(),
            static::FROM        => $this->getIsFrom(),
            static::JOB_TITLE   => $this->getJobTitle(),
            static::TO          => $this->getUntilTo(),
        ];
    }
}
