<?php

namespace App\Entity\History;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\History\WorkingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Working extends Abstraction
{
    /**
     * @ORM\Column(type="integer", unique=true)
     */
    protected $rank;

    public function getRank(): ?string
    {
        return $this->rank;
    }

    public function setRank(string $rank): self
    {
        $this->rank = $rank;

        return $this;
    }
}
