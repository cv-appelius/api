<?php

namespace App\Entity;

use App\Traits\Entity\name;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SkillsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Skills extends GenericEntity
{
    use name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $percentage;

    public function getPercentage(): ?int
    {
        return $this->percentage;
    }

    public function setPercentage(int $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function jsonData(): array
    {
        return [
            static::NAME    => $this->getName(),
            static::PERCENT => $this->getPercentage(),
        ];
    }
}
